import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActionSheetController, AlertController } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import axios from 'axios';
import config from 'src/config';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {

  public carts: any [];
  public baseUrl= config.BASE_URL;

  constructor(private router: Router,
    // private storage: Storage,
    private alertCtrl: AlertController,
    public navCtrl: NavController,) { }

  async getCart(){
  
    axios.get(`${config.API_URL}/cart`).then(res => {
      const data = res.data;
      if(data.error) {
        this.alertCtrl.create({
          header: 'Error',
          message: data.error_message,
          buttons: ['Ok']
        }).then(alert => {
          alert.present();
        });
      } else {
        this.carts = data.carts;
      }
    })
  }

  ngOnInit() {
    this.getCart();
  }

}
