import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import axios from 'axios';
import config from 'src/config';
import { ActionSheetController, AlertController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-detail-product',
  templateUrl: './detail-product.page.html',
  styleUrls: ['./detail-product.page.scss'],
})
export class DetailProductPage implements OnInit {


  constructor(private router: Router,
    private route: ActivatedRoute,
    private alertCtrl: AlertController,
    public navCtrl: NavController,) { }

  
  public id;
  public isLoading = true;
  public products: any [];
  public baseUrl= config.BASE_URL;
  public photo ='';
  public name = '';
  public harga = '';
  public description = '';
  public stok = '';

  showProduct(){
    
    this.route.params.subscribe(params => {
      this.id = params['productId']; 
    });
  
      this.isLoading = true;

      axios.get(`${config.API_URL}/product/show/${this.id}`).then(res => {
        const data = res.data;
        if(data.error) {
          this.alertCtrl.create({
            header: 'Error',
            message: data.error_message,
            buttons: ['Ok']
          }).then(alert => {
            alert.present();
          });
        } else {
          this.isLoading = false;
          this.name = data.product.name;
          this.photo = data.product.photo;
          this.harga = data.product.harga;
          this.description = data.product.description;
          this.stok = data.product.stok;
        }
      })
    

  }

  async cartAlertPrompt() {
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: 'Masukkan Ke Cart ?',
      subHeader: 'Silahkan masukkan jumlah produk!',
      inputs: [
        {
          name: 'qty',
          type: 'number'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            this.addToCart(data.qty)
            console.log('Confirm Ok');
          }
        }
      ]
    });
  
    await alert.present();
  }

  async addToCart(qty){
    const formData = new FormData();
    formData.append('qty', qty);
    formData.append('product_id', this.id);
    formData.append('status', 'cart');

    axios.post(`${config.API_URL}/cart/create`, formData, {
    }).then(response => {
      const jsonResponse = response.data;
      this.isLoading = false;
      //allert if there is an error
      if (jsonResponse.error) {
        this.alertCtrl.create({
          header: 'Error',
          message: jsonResponse.error_message,
          buttons: ['Ok']
        }).then(alert => {
          alert.present();
        });
      } else {
        // alert if Success
        this.alertCtrl.create({
          header: 'Success Masuk Ke Cart',
          message: jsonResponse.message,
          buttons: ['Ok']
        }).then(alert => {
          alert.present().then(() => {
            this.navCtrl.pop().then(() => {
              this.navCtrl.navigateRoot('cart');
            });
          });
        });
      } 
    }).catch(error => {
      console.error(error);
      this.isLoading = false;
      //alert to show catch error
      this.alertCtrl.create({
        header: 'Error',
        message: 'Sorry, something went wrong. Please check your internet connection and try again later.',
        buttons: ['Ok']
      }).then(alert => {
        alert.present();
      });
    });
  }

  ngOnInit() {
    this.showProduct();
  }
 
}
