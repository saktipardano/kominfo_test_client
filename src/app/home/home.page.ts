import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ActionSheetController, AlertController } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import axios from 'axios';
import config from 'src/config';
// import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

    public isLoading = true;
    public products: any [];
    public baseUrl= config.BASE_URL;

    constructor(private router: Router,
    // private storage: Storage,
    private alertCtrl: AlertController,
    public navCtrl: NavController,
    private actionSheetController: ActionSheetController,) {}

    async getProduct(){
      this.isLoading = true;

      axios.get(`${config.API_URL}/product`).then(res => {
        const data = res.data;
        if(data.error) {
          this.alertCtrl.create({
            header: 'Error',
            message: data.error_message,
            buttons: ['Ok']
          }).then(alert => {
            alert.present();
          });
        } else {
          this.isLoading = false;
          this.products = data.product;
        }
      })
    }

    gotoDetailProduct(productId){
      this.router.navigate(['detail-product', {productId:productId}]);
    }

    ngOnInit(){
      this.getProduct();
    }
}
