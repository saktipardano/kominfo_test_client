export default {
    //API_URL: 'https://matchapp.io/api',
    API_URL: 'https://kominfoserver.herokuapp.com/api',
    // API_URL: 'http://sysemp.herokuapp.com/api',
    BASE_URL: 'https://kominfoserver.herokuapp.com/',
    STORAGE_TOKEN_IDENTIFIER: 'MATCHAPP_TOKEN',
    STORAGE_USER_IDENTIFIER: 'USER_DATA'
};